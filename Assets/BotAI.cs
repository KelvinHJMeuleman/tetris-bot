using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotAI : MonoBehaviour
{

    Game game;
    Transform[,] blockGrid;

    public BotAI(Game game)
    {
        this.game = game;
    }

    public void blockSpawned(block currentBlock) {
        bool spotFound = false;
        int finalSpotX = 0, finalSpotY = 0, rotationTimes = 0;
        for (int y = 0; y < Game.height && !spotFound; y++)
        {
            for (int x = 0; x < Game.width && !spotFound; x++)
            {
                //lege plek gevonden
                if (game.grid[x, y] == null)
                {
                    //voor elke rotatie van de blok
                    for (int i = 0; i < 4 && !spotFound; i++)
                    {
                        //loop door elke sprite
                        foreach (Transform item in currentBlock.rig.transform)
                        {
                            //pak de positie van de sprite relative van de rig (dit geeft tussen 0-3)
                            int itemX = Mathf.FloorToInt(item.localPosition.x);
                            int itemY = Mathf.FloorToInt(item.localPosition.y);
                            //verander coordinaten vergeleken met de rotation
                            switch(item.eulerAngles.z)
                            {
                                case 90:
                                    int cache = itemY;
                                    itemY = itemX;
                                    itemX = -cache;
                                    break;
                                case 180:
                                    itemY = -itemY;
                                    itemX = -itemX;
                                    break;
                                case 270:
                                    cache = itemY;
                                    itemY = -itemX;
                                    itemX = cache;
                                    break;
                            }
                            //kijk of alle sprites binnen bounds is, X is de houdige plek geselecteert, itemX is de relative positie vergeleken met de plek
                            if (x + itemX < 0 || x + itemX > Game.width-1 || y + itemY < 0 || y + itemY > Game.height)
                            {
                                break;
                            }
                            //kijk of er plek is in de blokken ernaast
                            if (game.grid[x + itemX, y + itemY] != null)
                            {
                                break;
                            }
                            //als er plek is, zet dit zolang op de final spot totdat we bij de laatste sprite zijn
                            else
                            {
                                finalSpotX = x;
                                finalSpotY = y;
                                rotationTimes = i;
                                if (item == currentBlock.rig.transform.GetChild(currentBlock.rig.transform.childCount-1))
                                {
                                    //bij de laatste sprite betekent het dat wij een plek hebben gevonden
                                    spotFound = true;
                                }
                            }
                        }
                        if (!spotFound)
                        {
                            
                            print("Turning");
                            currentBlock.rig.transform.eulerAngles += new Vector3(0, 0, 90);
                        }
                    }

                    if (spotFound)
                    {
                        x = x - Mathf.FloorToInt(currentBlock.rig.transform.localPosition.x);
                        y = y - Mathf.FloorToInt(currentBlock.rig.transform.localPosition.y);
                        print(finalSpotX + "  " + finalSpotY + "  " + rotationTimes);
                        controlBlock(finalSpotX, finalSpotY, rotationTimes, currentBlock);
                    }
                }
                

            }



        }
    } 
       

  


    public void controlBlock(int x, int y, int rotate, block block)
    {
        //block.rotate(rotate);
        block.move(x - 7);
        block.down();
    }

    public void findSpot()
    {
      
       

    }
   
    




    public void checkSpot(int x , int y)
    {
        int blockX = 0, blockY = 0;
        //we need to know if this spot can fit a block
    

        
        
        
    }




}
