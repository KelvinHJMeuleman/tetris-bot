using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class block : MonoBehaviour
{

    float timer;
    public GameObject rig;
    private bool movable = true;
    Game game;
    // Start is called before the first frame update
    void Start()
    {
        new WaitForSeconds(0);
        game = FindObjectOfType<Game>();
    }

   


    public bool isValid()
    {
       
        foreach(Transform subBlock in rig.transform)
        {

            if (subBlock.transform.position.x > Game.width ||
                subBlock.transform.position.x < 0 ||
                subBlock.transform.position.y < 0)
            {
                return false;
            }
            if (subBlock.position.y < Game.height && game.grid[Mathf.FloorToInt(subBlock.position.x), Mathf.FloorToInt(subBlock.position.y)] != null)
            {
                return false;
            }
        }
        
        return true;

    }

    public void registerBlock()
    {
        foreach (Transform subBlock in rig.transform)
        {
            if (Mathf.FloorToInt(subBlock.position.y) < Game.height)
            {
                game.grid[Mathf.FloorToInt(subBlock.position.x), Mathf.FloorToInt(subBlock.position.y)] = subBlock;
                game.scoreBlock();
            }
            else
            {
                game.finishGame();
            }
            
        }
        game.clearRow();
    }

    // Update is called once per frame
    void Update()
    {
        if (movable)
        {
            timer += Time.deltaTime;
           
            if ( Input.GetKey("down") && timer > Game.quickDrop)
            {
                gameObject.transform.position -= new Vector3(0, 1, 0);
                timer = 0;

                if (!isValid())
                {
                    movable = false;
                    gameObject.transform.position += new Vector3(0, 1, 0);
                    registerBlock();
                    game.SpawnBlock();
                }
            }
            else if (timer > Game.dropTime)
            {
                gameObject.transform.position -= new Vector3(0, 1, 0);
                timer = 0;

                if (!isValid())
                {
                    movable = false;
                    gameObject.transform.position += new Vector3(0, 1, 0);
                    registerBlock();
                    game.SpawnBlock();
                }
            }


            if (Input.GetKeyDown("left"))
            {
                gameObject.transform.position -= new Vector3(1, 0, 0);
                if (!isValid())
                {
                    gameObject.transform.position += new Vector3(1, 0, 0);
                }
            }
            else if (Input.GetKeyDown("right"))
            {
                gameObject.transform.position += new Vector3(1, 0, 0);
                if (!isValid())
                {
                    gameObject.transform.position -= new Vector3(1, 0, 0);
                }
            }

            if (Input.GetKeyDown("z"))
            {
                rig.transform.eulerAngles -= new Vector3(0, 0, 90);
                if (!isValid())
                {
                    rig.transform.eulerAngles += new Vector3(0, 0, 90);
                }
            }
            else if (Input.GetKeyDown("x"))
            {
                rig.transform.eulerAngles += new Vector3(0, 0, 90);
                if (!isValid())
                {
                    rig.transform.eulerAngles -= new Vector3(0, 0, 90);
                }
            }

        }
    }

    internal void down()
    {
        
            if (timer > Game.quickDrop)
            {
                gameObject.transform.position -= new Vector3(0, 1, 0);
                timer = 0;
            }
        
    }

    internal void move(int x)
    {
       
            gameObject.transform.position += new Vector3(x, 0, 0);

    }

    internal void rotate(int rotate)
    {
        for (int i = 0; i < rotate; i++)
        {
            rig.transform.eulerAngles -= new Vector3(0, 0, 90);
        }
        
    }

    public void simDown()
    {
        while (!isValid())
        {
            rig.transform.position += new Vector3(0, 1, 0);
        }
    }
}
