using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    private Text t;
    // Start is called before the first frame update
    void Start()
    {
        Transform child = transform.Find("Text");
        t = child.GetComponent<Text>();
    }

    public void updateScore(double score)
    {
       
        t.text = score.ToString();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
