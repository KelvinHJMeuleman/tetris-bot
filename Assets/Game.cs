using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static float dropTime = 0.9f;
    public static float quickDrop = 0.1f;
    public static int width = 15, height = 30;
    public block[] blocks;
    public Transform[,] grid = new Transform[width, height];
    float score = 0;
    private bool gameStarted = true;
    private UI ui;
    private block currentBlock;
    private BetterAI bot;
    private bool isClone;
    // Start is called before the first frame update

    void Start()
    {
        bot = FindObjectOfType<BetterAI>();
        ui = FindObjectOfType<UI>();
        SpawnBlock();

    }



    public void SpawnBlock()
    {
        float random = Random.Range(0, 1f);
        random *= blocks.Length;
        currentBlock = Instantiate(blocks[Mathf.FloorToInt(random)]);
        if (score != 0)
        {
            ui.updateScore(score);
        }
        bot.act(currentBlock);

    }

    public void scoreBlock()
    {
        score += 1;
    }

    public void clearRow()
    {

        float scoreRow = 0;
        for (int y = 0; y < height; y++)
        {
            if (checkRow(y))
            {
                scoreRow += 100 + (scoreRow * 1.5f);
                    deleteRow(y);
                    moveRowsDown(y);
                    y--; 
            }
        }
        score += scoreRow;

    }



    public void deleteRow(int y)
    {
        for (int x = 0; x < width; x++)
        {

            Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }
    }

    public void moveRowsDown(int deletedRow)
    {
        for (int y = deletedRow; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (grid[x, y] != null)
                {
                    Transform gridPos = grid[x, y];
                    grid[x, y] = null;
                    gridPos.position -= new Vector3(0, 1, 0);
                    grid[x, y - 1] = gridPos;
                }
            }
        }

    }

    internal void finishGame()
    {

        gameStarted = false;
    }


    //returns false when line not complete
    public bool checkRow(int y)
    {
        for (int x = 0; x < width; x++)
        {

            if (grid[x, y] == null)
            {
                return false;
            }
        }
        return true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Transform[,] clone()
    {
        return (Transform[,]) grid.Clone();
      
    }

   /**
    public Stats down(int x)
    {
        currentBlock.rig.transform.position = new Vector3(x, 0);
        currentBlock.simDown();

        return generateStats();

    }
   **/
    
}
