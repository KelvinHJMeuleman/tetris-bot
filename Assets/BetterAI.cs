using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterAI : MonoBehaviour
{

    [Header("Game")]
    [SerializeField]
    public Game game;
    [Header("Constants")]
    [SerializeField]
    public static float gapConstant = -0.53f;
    [SerializeField]
    public static float pointConstant = 0.1f;
    [SerializeField]
    public static float heightConstant = -0.36f;

    public struct TetrominoSpot : IComparable
    {
        public int y;
        public int x;
        public int rotation;
        public float utility;
        public Transform[,] grid;

        public TetrominoSpot(int x, int y, int rotation, float utility, Transform[,] grid)
        {
            this.x = x;
            this.y = y;
            this.rotation = rotation;
            this.utility = utility;
            this.grid = grid;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            TetrominoSpot otherAgent = (TetrominoSpot)obj;
            return otherAgent.utility.CompareTo(utility);
        }
        override
        public String ToString()
        {
            return x + " " + y + " " + rotation + " " + utility;
        }
    }

    public struct BlockPos
    {
        public int y;
        public int x;
        public BlockPos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    
    public class Stats
    {
        public float gaps;
        public float height;
        public float addedPoints;
        public float possible;
        public Transform[,] grid;
        override
        public String ToString()
        {
            return gaps + " " + height + " " + addedPoints + " " + possible + "\n " + (height * heightConstant + addedPoints * pointConstant + gaps * gapConstant) + possible;
        }
    }

    public void act(block currentBlock)
    {
        //simulate the Tetris fall down

        List<TetrominoSpot> spots = new List<TetrominoSpot>();
        for (int x = 0; x < Game.width - 1; x++)
        {
            currentBlock.rig.transform.eulerAngles = new Vector3(0, 0, 0);
            for (int i = 0; i < 4; i++)
            {
                Transform[,] simGame = game.clone();
                TetrominoSpot spot = new TetrominoSpot();
                spot.x = x;

                Stats stats = Simulate(simGame, x, i, currentBlock, ref spot);
                if (stats != null)
                {
                    spot.utility = CalculateUtility(stats);
                    spots.Add(spot);
                }
                currentBlock.rig.transform.eulerAngles += new Vector3(0, 0, 90);
            }


        }
        spots.Sort();
        print(spots[0].ToString());
        printGrid(spots[0].grid);
        MoveBlock(spots[0], currentBlock);

    }


    public Stats Simulate(Transform[,] grid, int x, int rotation, block currentBlock, ref TetrominoSpot spot)
    {
        //
        float possible = 1;
        int y = 0;
        Stats stats = new Stats();
        Transform[,] debugGrid;
        BlockPos[] positions;
        bool placedBlock = false;
        while (!placedBlock && y < Game.height -1)
        {
            debugGrid = new Transform[Game.width, Game.height];
            positions = new BlockPos[4];
            int yCorrection = 0;
            List<BlockPos> simulatedPos = new List<BlockPos>();
            for (int i = 0; i < currentBlock.rig.transform.childCount; i++)
            {
                Transform item = currentBlock.rig.transform.GetChild(i);
                int itemX = Mathf.FloorToInt(item.localPosition.x);
                int itemY = Mathf.FloorToInt(item.localPosition.y);
                switch (item.eulerAngles.z)
                {
                    case 90:
                        int cache = itemY;
                        itemY = itemX;
                        itemX = -cache;
                        break;
                    case 180:
                        itemY = -itemY;
                        itemX = -itemX;
                        break;
                    case 270:
                        cache = itemY;
                        itemY = -itemX;
                        itemX = cache;
                        break;
                }
                positions[i] = new BlockPos(itemX, itemY);
                if (itemY < yCorrection)
                {
                    yCorrection = itemY;
                }

            }



            for (int i = 0; i < currentBlock.rig.transform.childCount; i++)
            {
                BlockPos block = positions[i];
                int itemX = block.x;
                int itemY = block.y - yCorrection;
                //addBlockMiddle(debugGrid, item, itemX, itemY);

                if (itemX + x < 0 || itemX + x > Game.width - 1 || itemY + y < 0 || itemY + y > Game.height - 1)
                {
                    y++;
                    break;
                }
                //check if there aren't any blocks in those positions
                if (game.grid[itemX + x, itemY + y] != null)
                {
                    //if there is a block in the way, maybe it can fall ontop of the block and still be a decent choice
                    y++;
                    break;
                }
                //add the position to the list
                simulatedPos.Add(new BlockPos(itemX, itemY));

            }
            //if the list is the same as the amount of blocks in a tetromino, then all the blocks fit
            if (simulatedPos.Count == currentBlock.rig.transform.childCount)
            {
                //we can place every block
                for (int i = 0; i < simulatedPos.Count; i++)
                {
                    BlockPos block = simulatedPos[i];
                    //in the grid
                    grid[x + block.x, y + block.y] = currentBlock.rig.transform.GetChild(i);
                    placedBlock = true;
                }

            }
            else
            {
                y++;
            }
        }
        if (placedBlock)
        {
           
            spot.y = y;
            spot.rotation = rotation;
            //printGrid(grid);
            //printGrid(debugGrid);
            
            //print(spot);
            //print(stats);
            stats.possible = possible;
            spot.grid = grid;
            return GenerateStats(grid, stats);
        }


        return null;
    }

    public void printGrid(Transform[,] grid)
    {
        string i = "";
        for (int y = Game.height - 1; y >= 0; y--)
        {
            i = i + "\n";
            for (int x = 0; x < Game.width; x++)
            {
                string j = " ";
                if (grid[x, y] != null)
                {
                    j = "X";
                }


                i = i + " [ " + j + " ]";

            }
        }
        print(i);
    }

    private float CalculateUtility(Stats p)
    {
        return (p.height * heightConstant + p.addedPoints * pointConstant + p.gaps * gapConstant) + p.possible;
    }

    public Stats GenerateStats(Transform[,] grid, Stats stats)
    {
        int gaps = 0;
        int blocksHeight = 0;
        float points = 4;

        float scoreRow = 0;

        for (int y = 0; y < Game.height; y++)
        {
            bool blockAreInRow = false;
            bool rowFilled = true;
            for (int x = 0; x < Game.width; x++)
            {

                if (grid[x, y] == null)
                {
                    rowFilled = false;
                    if (y < Game.height - 1 && grid[x, y + 1] != null)
                    {
                        gaps++;
                    }
                }
                else
                {
                    blockAreInRow = true;
                }
            }
            if (rowFilled)
            {
                scoreRow += 100 + (scoreRow * 1.5f);
            }
            if (blockAreInRow)
            {
                blocksHeight++;
            }
        }

        points += scoreRow;

        stats.gaps = gaps;
        stats.height = blocksHeight;
        stats.addedPoints = points;
        return stats;
    }

    public void MoveBlock(TetrominoSpot spot, block currentBlock)
    {
        currentBlock.transform.position = new Vector3(spot.x, Game.height);
        currentBlock.rig.transform.eulerAngles = new Vector3(0, 0, 90 * spot.rotation);
    }

    public void addBlockMiddle(Transform[,] debugGrid, Transform block, int x, int y)
    {

        debugGrid[6 + x, 10 + y] = block;

    }
}
