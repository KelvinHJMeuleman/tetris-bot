using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotAI_new : MonoBehaviour
{

    struct TetrominoSpot : IComparable
    {
        public int y { get; }
        public int x { get; }
        public int rotation { get; }
        public float utility;

        public TetrominoSpot(int x, int y, int rotation, float utility)
        {
            this.x = x;
            this.y = y;
            this.rotation = rotation;
            this.utility = utility;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            TetrominoSpot otherAgent = (TetrominoSpot)obj;
            return otherAgent.utility.CompareTo(utility);
        }
        public String ToString()
        {
            return x + " " + y + " " + rotation + " " + utility;
        }
    }
    [Header("Game")]
    [SerializeField]
    public Game game;   
    [Header("Constants")]
    [SerializeField]
    public float gapConstant = -0.33f;
    [SerializeField]
    public float pointConstant = 0.1f;
    

    public void blockSpawned(block currentBlock)
    {
        List<TetrominoSpot> spots = new List<TetrominoSpot>();
        for (int y = 0; y < Game.height; y++)
        {
            for (int x = 0; x < Game.width; x++)
            {
                //lege plek gevonden
                if (game.grid[x, y] == null)
                {
                    int gaps;

                    //for every rotation we check if it is inbounds and doesn't interfere with other blocks
                    //we also calculate the utilty points
                    for (int i = 0; i < 4; i++)
                    {
                        if (checkSpot(x, y, currentBlock))
                        {
                            
                            gaps = calGaps(x, y, i, currentBlock);
                            int points = calPoints(x, y, i, currentBlock);

                            spots.Add(new TetrominoSpot(x, y, i, calUtility(gaps, points)));
                            currentBlock.rig.transform.eulerAngles += new Vector3(0, 0, 90);
                        }
                    }
                }
            }
        }
        spots.Sort();
        print(spots[0].ToString());
    }

    private float calUtility(int gaps, int points)
    {
        float utility = 0;
        utility = gaps * gapConstant + points * pointConstant;
        return utility;
    }

    private bool checkSpot(int x, int y ,block currentBlock)
    {
        bool possible = true;
        foreach (Transform item in currentBlock.rig.transform)
        {
            //pak de positie van de sprite relative van de rig (dit geeft tussen 0-3)
            int itemX = Mathf.FloorToInt(item.localPosition.x);
            int itemY = Mathf.FloorToInt(item.localPosition.y);
            //verander coordinaten vergeleken met de rotation
            switch (item.eulerAngles.z)
            {
                case 90:
                    int cache = itemY;
                    itemY = itemX;
                    itemX = -cache;
                    break;
                case 180:
                    itemY = -itemY;
                    itemX = -itemX;
                    break;
                case 270:
                    cache = itemY;
                    itemY = -itemX;
                    itemX = cache;
                    break;
            }
            //change relative position to the actual position of the sprite
            //check if the sprite is out of bounds
            if (itemX + x < 0 || itemX + x > Game.width - 1 || itemY + y < 0 || itemY + y > Game.height - 1)
            {
                possible = false;
                break;
            }
            //check if there aren't any blocks in those positions
            if (game.grid[itemX + x, itemY + y] != null)
            {
                possible = false;
                break;
            }
           
        }
        return possible;
    }

    private int calGaps(int x, int y, int rotation,block currentBlock)
    {
        int gaps = 0;
        //first check the row under the block
        //the tetrominos are built so that 0 has the highest Y coordinate and the last index has the highest X coordinate
        if(y != 0)
        {
            for (int i = 0; i < currentBlock.rig.transform.childCount -1; i++)
            {
                if(game.grid[i, y - 1] == null)
                {
                    gaps++;
                }
            }
        }
        //now that we considered all the grids below the tetromino
        //we need to consider the grids around the tetromino

        return gaps;
    }

    private int calPoints(int x, int y, int rotation ,block currentBlock)
    {
        //placing a tetromino gives 4 points
        int points = 4;
        //finishing a row gives 100 points
        bool rowFinished = true;
        int sizeX = 0;
        foreach (Transform item in currentBlock.rig.transform)
        {
           if(item.position.y == y && item.localPosition.x > sizeX)
            {
                sizeX = Mathf.FloorToInt(item.localPosition.x);
            }
        }



        for (int i = 0; i < Game.width; i++)
        {
            if (x + sizeX > i || x <= i)
            {
                if (game.grid[x, y] == null)
                {

                    rowFinished = false;
                    break;
                }
            }
        }
        if (rowFinished)
        {
            points += 100;
        }


        return points;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
